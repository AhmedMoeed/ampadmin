import { Component, OnInit, Inject, ViewEncapsulation } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

@Component({
  selector: 'app-help',
  templateUrl: './help.component.html',
  styleUrls: ['./help.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class HelpComponent implements OnInit {
  private title = null;
  constructor(
    public dialogRef: MatDialogRef<HelpComponent>,
    @Inject(MAT_DIALOG_DATA) public options: any) { }

  ngOnInit() {

    switch (this.options) {
      case 'practice_info':
        this.title = "Practice Information";
        break;
      case 'office_hours':
        this.title = "Office Hours";
        break;
      case 'social_information':
        this.title = "Social Information";
        break;
      case 'emr_info':
        this.title = "EMR Information";
        break;
      case 'services':
        this.title = "Service";
        break;
      case 'automated_messaging':
        this.title = "Automated Messaging";
        break;
      case 'logo_style':
        this.title = "Logo Style";
        break;
      case 'logo':
        this.title = "Logo";
        break;
    }

  }

  close() {
    this.dialogRef.close();
  }

}
