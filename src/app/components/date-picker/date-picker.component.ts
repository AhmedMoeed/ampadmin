import { Component, OnInit, ViewChild, ElementRef, Input, Output, EventEmitter } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-date-picker',
  templateUrl: './date-picker.component.html',
  styleUrls: ['./date-picker.component.scss']
})
export class DatePickerComponent implements OnInit {
  form: FormGroup;
  @ViewChild('datepick', { static: false }) datepick: ElementRef;
  @Output('ngModel') ngModel = new EventEmitter<any>();
  private model = '';
  private filled = false;

  constructor() {

    this.form = new FormGroup({
      control: new FormControl(null, [])
    });
    this.form.get('control').valueChanges.subscribe((value) => {
      if (this.datepick['selects'].d.value != null || this.datepick['selects'].m.value != null || this.datepick['selects'].y.value != null) {
        this.filled = true;
        if (this.datepick['selects'].d.value != null && this.datepick['selects'].m.value != null && this.datepick['selects'].y.value != null) {
          this.model = this.datepick['selects'].d.value + '/' + this.datepick['selects'].m.value + '/' + this.datepick['selects'].y.value;
          this.ngModel.emit(this.model);
        } else {
          this.ngModel.emit('');

        }
      } else {
        this.filled = false;
      }
    });

  }

  ngOnInit() {
  }

}
