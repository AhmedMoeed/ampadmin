import { Component, OnInit, Inject, ViewEncapsulation } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { AuxService } from '../../auxilaries/aux_.service';


@Component({
  selector: 'app-terms',
  templateUrl: './terms.component.html',
  styleUrls: ['./terms.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class TermsComponent implements OnInit {

  constructor(
    private aux: AuxService,
    public dialogRef: MatDialogRef<TermsComponent>,
    @Inject(MAT_DIALOG_DATA) public options: any
  ) { }


  ngOnInit() {
    console.log(this.options)
  }

  continue() {
    // this.scrollToTop();
    this.dialogRef.close();
  }

}
