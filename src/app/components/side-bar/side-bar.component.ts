import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Router } from '@angular/router';
import { Location } from '@angular/common';
import { CredentialService } from 'src/app/services/credentials/credential.service';

@Component({
  selector: 'app-side-bar',
  templateUrl: './side-bar.component.html',
  styleUrls: ['./side-bar.component.scss']
})
export class SideBarComponent implements OnInit {
  @Input() display: boolean;
  @Output() close: EventEmitter<any> = new EventEmitter<any>()
  public currentPage = '';
  public user: any;
  constructor(public router: Router, public loc: Location, public credentials: CredentialService) {
    this.user = this.credentials.user;
    this.credentials.sessionStatus.subscribe(value => {
      this.user = value
    })
  }

  ngOnInit() {
    this.currentPage = this.loc.path();
  }
  isSettingsRoute() {
    if (this.loc.path() == '/home/settings')
      return true;
    else if (this.loc.path() == '/forms')
      return true;
    else if (this.loc.path() == '/family')
      return true;
    else
      return false;
  }

  goTo(page) {
    console.log(this.loc.path());
    console.log(page)
    if (page == 'login') {
      this.currentPage = '/login';
      this.router.navigate(["/login"]);
    } else if (page == 'plan' && this.user == null) {
      this.currentPage = '/plan';
      this.router.navigate(["/plan"]);
    } else if (this.user != null) {

      if (page == 'scheduling') {
        this.currentPage = '/scheduling';
        this.router.navigate(['/scheduling'])
      }
      if (page == 'chat') {
        this.currentPage = '/chat';
        this.router.navigate(["/chat"]);
      } else if (page == 'dashboard') {
        this.currentPage = '/home';
        this.router.navigate(["home"]);
      } else if (page == 'settings') {
        this.currentPage = '/home/settings';
        this.router.navigate(["home/settings"]);
      } else if (page == 'forms') {

        if (this.currentPage == '/formsN') {
          this.router.navigate(["/forms"]);
          this.currentPage = '/forms';
        } else if (this.currentPage == '/forms') {
          this.router.navigate(["/formsN"]);
          this.currentPage = '/formsN';
        } else {
          this.currentPage = '/forms';
          this.router.navigate(["/forms"]);
        }

      }
      else if (page == 'support') {
        this.currentPage = '/home/support';
        this.router.navigate(["home/support"]);
      }
    }
  }

  hide() {
    if (window.innerWidth < 720) {
      this.close.emit('closeIt');
    }
  }

  logout() {
    this.credentials.logout();
    this.router.navigate(['/auth']);
  }

}
