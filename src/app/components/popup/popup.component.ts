import { Component, OnInit, Inject, ViewEncapsulation } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { AuxService } from '../../auxilaries/aux_.service';


@Component({
  selector: 'app-popup',
  templateUrl: './popup.component.html',
  styleUrls: ['./popup.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class PopupComponent implements OnInit {

  constructor(
    private aux: AuxService,
    public dialogRef: MatDialogRef<PopupComponent>,
    @Inject(MAT_DIALOG_DATA) public options: any
  ) { }


  ngOnInit() {
    console.log(this.options)
  }

  continue() {
    // this.scrollToTop();
    this.dialogRef.close();
  }

}
