import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HelpbarComponent } from './helpbar.component';

describe('HelpbarComponent', () => {
  let component: HelpbarComponent;
  let fixture: ComponentFixture<HelpbarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HelpbarComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HelpbarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
