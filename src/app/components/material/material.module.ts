import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MatRadioButton, MatRadioGroup } from '@angular/material/radio';

import { MatButtonModule, MatTabsModule, MatDialogModule, MatRippleModule, MatListModule, 
  MatProgressSpinnerModule, MatExpansionModule, MatDatepickerModule, MatSlideToggle, 
  MatNativeDateModule, MatCardModule, MatSelectModule, MAT_DATE_FORMATS, DateAdapter, MAT_DATE_LOCALE,
  MatAccordion } from '@angular/material/';

import { MatInputModule} from '@angular/material/input';
import { MomentDateAdapter } from '@angular/material-moment-adapter';

export const EXPIRY_FORMAT = {
  parse: {
    dateInput: 'MM/YYYY',
  },
  display: {
    dateInput: 'MM/YYYY',
    monthYearLabel: 'MMM YYYY',
    dateA11yLabel: 'LL',
    monthYearA11yLabel: 'MMMM YYYY',
  },
};

@NgModule({
  declarations: [MatRadioButton, MatRadioGroup, MatSlideToggle],
  imports: [
    CommonModule,
    MatSelectModule,
    MatButtonModule,
    MatTabsModule,
    MatDialogModule,
    MatRippleModule,
    MatListModule,
    MatProgressSpinnerModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatDatepickerModule,
    MatTabsModule,
    MatInputModule,
    MatExpansionModule,
    MatCardModule
  ],
  exports: [
    MatButtonModule,
    MatTabsModule,
    MatDialogModule,
    MatRippleModule,
    MatListModule,
    MatSelectModule,
    MatProgressSpinnerModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatRadioButton,
    MatInputModule,
    MatRadioGroup,
    MatSlideToggle,
    MatExpansionModule,
    MatCardModule
  ],
  providers: [
    { provide: DateAdapter, useClass: MomentDateAdapter, deps: [MAT_DATE_LOCALE] },
    { provide: MAT_DATE_FORMATS, useValue: EXPIRY_FORMAT },
  ]
})
export class MaterialModule { }
