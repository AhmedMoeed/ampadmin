import { Component, OnInit, Inject, ViewEncapsulation } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { AuxService } from '../../auxilaries/aux_.service';
import * as ClassicEditor from '@ckeditor/ckeditor5-build-classic';


@Component({
  selector: 'app-question',
  templateUrl: './question.component.html',
  styleUrls: ['./question.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class QuestionComponent implements OnInit {
  public statement = '';
  private selectedType = "text";
  public Editor = ClassicEditor;
  private hours = '00';
  private minutes = '45';
  private configRich = { toolbar: ['heading', '|', 'bold', 'italic', 'link', 'bulletedList', 'numberedList', 'blockQuote'] }
  private config = { toolbar: [], allowedContent: false }

  multiChoices = [
    {
      'option': '',
      'value': '',
      'error': false
    }
  ];



  constructor(
    private aux: AuxService,
    public dialogRef: MatDialogRef<QuestionComponent>,
    @Inject(MAT_DIALOG_DATA) public options: any
  ) { }


  ngOnInit() {

    this.statement = this.options.original;
  }
  removeChoice(i) {
    this.multiChoices.splice(i, 1)
  }
  addMultiChoice() {
    console.log(this.multiChoices)
    this.multiChoices.push({ 'option': '', 'value': '', 'error': false });
  }
  cancel() {
    this.dialogRef.close({ 'success': false });
  }
  done() {
    console.log(this.statement);

    this.statement = this.statement.replace(new RegExp("<p>", 'g'), '');
    this.statement = this.statement.replace(new RegExp("</p>", 'g'), '');
    this.statement = this.statement.replace(new RegExp("&nbsp;", 'g'), ' ');
    console.log(this.statement);
    if (this.statement.length < 5) {
      this.aux.showAlert('Title must be of at least 5 characters', "Error!");
    } else {

      if (this.options.from != undefined && this.options.from == 'service' && this.options.type == 'new') {
        this.dialogRef.close({ 'success': "true", "statement": this.statement, "hour": this.hours, "minutes": this.minutes });
        return;
      }
      else if (this.options.type == 'new') {
        switch (this.selectedType) {
          case 'radio':

            if (this.multiChoices.length < 2) {
              this.aux.showAlert('You need to enter at least two choices.', 'Error!');
              return;
            }

            let errorFound = false;
            for (let i = 0; i < this.multiChoices.length; i++) {
              if (this.multiChoices[i].option == '') {
                this.multiChoices[i].error = true
                errorFound = true;
              }
            }
            if (errorFound) {
              return;
            } else {
              let options = [];
              for (let i = 0; i < this.multiChoices.length; i++) {
                options.push(this.multiChoices[i].option)
              }
              this.dialogRef.close({ 'success': "true", "type": "radio", "statement": this.statement, "options": options });
            }
            return;
          default:
            this.dialogRef.close({ 'success': "true", "type": this.selectedType, "statement": this.statement });
            return;
        }
      } else {
        this.dialogRef.close({ 'success': "true", "type": this.selectedType, "statement": this.statement });
        return;
      }
    }

  }

  addTime(i) {
    let total = parseInt(this.hours) * 60;
    total += parseInt(this.minutes);

    if (total < 120) {
      total += 15;
      this.hours = parseInt((total / 60).toFixed(1)) + '';
      this.minutes = total % 60 + '';
      this.hours = '0' + this.hours;
      this.minutes == '0' ? this.minutes = '00' : this.minutes;
    }

  }
  removeTime(i) {
    let total = parseInt(this.hours) * 60;
    total += parseInt(this.minutes);

    if (total > 15) {
      total -= 15;
      this.hours = parseInt((total / 60).toFixed(1)) + '';
      this.minutes = total % 60 + '';
      this.hours = '0' + this.hours;
      this.minutes == '0' ? this.minutes = '00' : this.minutes;
    }
  }
}
