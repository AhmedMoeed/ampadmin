import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MaterialModule } from './material/material.module';
import { AlertComponent } from './modals/alert/alert.component';
import { FabComponent } from './fab/fab.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ProDetailsComponent } from './modals/pro-details/pro-details.component';
import { ThemesComponent } from './modals/themes/themes.component';
import { NgxMaterialTimepickerModule } from 'ngx-material-timepicker';
import { Picker } from './pipes/pipes.pipe';
import { NgxMaskModule } from 'ngx-mask';
import { CKEditorModule } from '@ckeditor/ckeditor5-angular';
import { ImageCropperModule } from 'ngx-image-cropper';
// import { ColorPickerModule } from 'ngx-color-picker';
import { QuestionComponent } from './question/question.component';
import { HelpComponent } from './help/help.component';
import { HelpbarComponent } from './helpbar/helpbar.component';
import { SidebarModule } from 'ng-sidebar';
import { ResizerComponent } from './resizer/resizer.component';
import { ComboDatepickerModule } from 'ngx-combo-datepicker';
import { DatePickerComponent } from './date-picker/date-picker.component';
import { PopupComponent } from './popup/popup.component';
import { TermsComponent } from './terms/terms.component';
@NgModule({
  declarations: [
    AlertComponent,
    FabComponent,
    ProDetailsComponent,
    ThemesComponent,
    Picker,
    QuestionComponent,
    HelpComponent,
    HelpbarComponent,
    ResizerComponent,
    DatePickerComponent,
    PopupComponent,
    TermsComponent
  ],
  imports: [
    ComboDatepickerModule,
    CKEditorModule,
    NgxMaterialTimepickerModule,
    CommonModule,
    FormsModule,
    MaterialModule,
    SidebarModule,
    ImageCropperModule,
    ReactiveFormsModule,
    // ColorPickerModule,
    NgxMaskModule.forChild(),
  ],
  exports: [
    AlertComponent,
    FabComponent,
    ProDetailsComponent,
    Picker,
    HelpbarComponent,
    DatePickerComponent
  ],
  entryComponents: [
    AlertComponent,
    ProDetailsComponent,
    ThemesComponent,
    QuestionComponent,
    HelpComponent,
    ResizerComponent,
    PopupComponent,
    TermsComponent
  ]
})
export class ComponentsModule { }
