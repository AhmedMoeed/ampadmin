import { Component, OnInit, Inject } from '@angular/core';
import { ImageCroppedEvent } from 'ngx-image-cropper';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';


@Component({
  selector: 'app-resizer',
  templateUrl: './resizer.component.html',
  styleUrls: ['./resizer.component.scss']
})
export class ResizerComponent implements OnInit {

  private imageChangedEvent: ImageCroppedEvent = null;
  private croppedImage = null;
  private file = null;
  private output = null;
  constructor(
    public dialogRef: MatDialogRef<ResizerComponent>,
    @Inject(MAT_DIALOG_DATA) public options: any

  ) {

    this.imageChangedEvent = options.event;


  }

  imageCropped(event: ImageCroppedEvent) {
    this.croppedImage = event.base64;
    this.output = event.file;
  }

  done() {
    this.dialogRef.close({ image: this.croppedImage, file: this.output });
    return;
  }

  ngOnInit() {
  }

}
