import { Injectable } from '@angular/core';
import { CredentialService } from 'src/app/services/credentials/credential.service';
import { Router, CanActivate } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class SessGuardService implements CanActivate {

  constructor(public auth: CredentialService, public router: Router) { }

  canActivate(): boolean {
    if (this.auth.loggedIn()) {
      if (!this.auth.selfSetup()) {
        this.router.navigate(['setup'])
      }
      return true;
    } else {
      this.router.navigate(['auth']);
      return false;
    }
  }
}
