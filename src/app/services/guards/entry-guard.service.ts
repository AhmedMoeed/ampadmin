import { Injectable } from '@angular/core';
import { CredentialService } from 'src/app/services/credentials/credential.service';
import { CanActivate, Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class EntryGuardService implements CanActivate {

  constructor(public auth: CredentialService, public router: Router) { }

  canActivate(): boolean {
    if (!this.auth.loggedIn()) {
      return true;
    }
    else {
      this.router.navigate(['home']);
      return false;
    }
  }
}

