import { TestBed } from '@angular/core/testing';

import { EntryGuardService } from './entry-guard.service';

describe('EntryGuardService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: EntryGuardService = TestBed.get(EntryGuardService);
    expect(service).toBeTruthy();
  });
});
