import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { CredentialService } from 'src/app/services/credentials/credential.service';

@Injectable({
  providedIn: 'root'
})
export class RouteGuardGuard implements CanActivate {
  constructor(public auth: CredentialService, public router: Router) { }
  canActivate() {
    if (this.auth.loggedIn()) {
      if (this.auth.selfSetup()) {
        this.router.navigate(['home'])
        return false;
      } else {
        return true;
      }

    } else {
      this.router.navigate(['auth'])
      return false;
    }
  }
}
