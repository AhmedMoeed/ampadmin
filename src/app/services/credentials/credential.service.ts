import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';
@Injectable({
  providedIn: 'root'
})
export class CredentialService {
  public token;
  public user: any;
  public sessionStatus = new Subject<any>();
  public formsForMe = true;
  public sub_patient = null;
  public sub_patient_email = null;
  constructor() {
    this.token = localStorage.getItem('pToken');
    var temp = localStorage.getItem('patient');
    if (temp) {
      this.user = JSON.parse(temp);
    }

  }

  updateUser(user) {
    localStorage.setItem('patient', JSON.stringify(user));
    this.user = user;
    console.log(this.user)
    this.sessionStatus.next(this.user);
  }
  setUser(user) {
    localStorage.setItem('patient', JSON.stringify(user));
    localStorage.setItem('pToken', user.access_token);
    this.token = user.access_token;
    this.user = user;
    this.sessionStatus.next(this.user);
  }

  logout() {
    localStorage.clear();
    this.user = null;
    this.token = null;
    this.sessionStatus.next(null)
  }

  loggedIn() {
    console.log(this.token)
    console.log(this.user)
    if (this.token && this.user) {
      return true;
    } else {
      return false;
    }
  }

  themeSelected() {
    if (this.user.theme == null || this.user.theme == '') {
      return false
    } else {
      return true
    }
  }

  selfSetup() {
    if (this.user.first_setup == true) {
      return true;
    } else
      return false;
  }
}
