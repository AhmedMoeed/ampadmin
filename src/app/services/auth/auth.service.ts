import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { ConstantsService } from 'src/app/init/constants.service';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  public providers = [];
  constructor(public http: Http, public constants: ConstantsService) { }

  signUp(params) {
    var url = this.constants.API_ENDPOINT + '/api/patient/auth/register';
    var response = this.http.post(url, params, {}).pipe(map(res => res.json()));
    return response;
  }

  pay(params) {
    var url = this.constants.API_ENDPOINT + '/api/dentist/auth/v2/pay-init';
    var response = this.http.post(url, params, {}).pipe(map(res => res.json()));
    return response;
  }

  login(params) {
    var url = this.constants.API_ENDPOINT + '/api/patient/auth/login';
    var response = this.http.post(url, params, {}).pipe(map(res => res.json()));
    return response;
  }
  fbLogin(params) {
    var url = this.constants.API_ENDPOINT + '/api/dentist/auth/fb/login';
    var response = this.http.post(url, params, {}).pipe(map(res => res.json()));
    return response;
  }
  gpLogin(params) {
    var url = this.constants.API_ENDPOINT + '/api/dentist/auth/google/login';
    var response = this.http.post(url, params, {}).pipe(map(res => res.json()));
    return response;
  }

  forgot(params) {
    var url = this.constants.API_ENDPOINT + '/api/dentist/auth/reset-password';
    var response = this.http.post(url, params, {}).pipe(map(res => res.json()));
    return response;
  }

  setupPatient(params) {
    var url = this.constants.API_ENDPOINT + '/api/patient/auth/setup';
    var response = this.http.post(url, params, {}).pipe(map(res => res.json()));
    return response;
  }
  addSubPatient(params) {
    var url = this.constants.API_ENDPOINT + '/api/patient/auth/add-sub-patient';
    var response = this.http.post(url, params, {}).pipe(map(res => res.json()));
    return response;
  }
  updateSubPatient(params) {
    var url = this.constants.API_ENDPOINT + '/api/patient/auth/update-sub-patient';
    var response = this.http.post(url, params, {}).pipe(map(res => res.json()));
    return response;
  }
  setupPatientForms(params) {
    var url = this.constants.API_ENDPOINT + '/api/patient/auth/setup-forms';
    var response = this.http.post(url, params, {}).pipe(map(res => res.json()));
    return response;
  }
  setupSubPatientForms(params) {
    var url = this.constants.API_ENDPOINT + '/api/patient/auth/setup-forms-sub-patient';
    var response = this.http.post(url, params, {}).pipe(map(res => res.json()));
    return response;
  }
  serviceProviders() {
    var url = this.constants.API_ENDPOINT + '/api/system/get-services';
    var response = this.http.get(url).pipe(map(res => res.json()));
    return response;
  }

  requestProvider(params) {
    var url = this.constants.API_ENDPOINT + '/api/system/add-service-request';
    var response = this.http.post(url, params, {}).pipe(map(res => res.json()));
    return response;
  }

  authenticateToken(params) {
    var url = this.constants.API_ENDPOINT + '/api/dentist/auth/authenticate-token';
    var response = this.http.post(url, params, {}).pipe(map(res => res.json()));
    return response;
  }
  emailValidity(params) {
    var url = this.constants.API_ENDPOINT + '/api/dentist/auth/email-validity';
    var response = this.http.post(url, params, {}).pipe(map(res => res.json()));
    return response;
  }

  setupTemplate(params) {
    var url = this.constants.API_ENDPOINT + '/api/dentist/auth/set-theme-data';
    var response = this.http.post(url, params, {}).pipe(map(res => res.json()));
    return response;
  }
  getZipData(params) {
    var url = this.constants.API_ENDPOINT + '/api/system/get-location';
    var response = this.http.post(url, params).pipe(map(res => res.json()));
    return response;
  }



}
