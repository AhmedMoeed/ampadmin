import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { Http, XHRBackend, RequestOptions, HttpModule } from '@angular/http';
import { FormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ComponentsModule } from './components/components.module';

import { AppRoutingModule  } from './app-routing.module';
import { AppComponent } from './app.component';

import { HttpFactory } from './services/httpInterceptor/httpFactory';
import { TestCComponent } from './test-c/test-c.component';
import { MaterialModule } from './components/material/material.module';

@NgModule({
  declarations: [
    AppComponent,
    TestCComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpModule,
    BrowserAnimationsModule,
    ComponentsModule,
    MaterialModule
  ],
  providers: [

  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
